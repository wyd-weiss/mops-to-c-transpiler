#!/bin/bash

set -e  # Exit on fail

# Check parameter
if [ $# -eq 0 ]
  then
    echo "Usage: compile-and-run-gcc.sh <name of .ass file>"
    exit
fi

# Compile program and execute it
echo "Transpiling $1..."
python3 mops-to-c.py $1 $1.c

echo "Compiling $1.c..."
gcc -o $1.a $1.c

echo "Executing $1.a..."
`realpath ${1}.a`

# Remove all generated files
echo "Cleaning up..."
rm $1.c
rm $1.a
echo "Done."
