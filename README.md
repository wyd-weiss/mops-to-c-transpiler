# MOPS to C transpiler

Python script that transpiles the educational model assemble MOPS (http://www.viktorianer.de/info/mops.html) to C.

# Using the script

You can execute the transpiler as follows:

    python3 <input assembler file> <output C file>

You can test the script using the example in the repository:

    python3 example.ass example.c

You can then compile the program using gcc (or any other C compiler) and execute it.

    gcc -o example.a example.c
    ./example.a

If you are using gcc, you can also start the bash script `transpile-and-run-gcc.sh`.
It transpiles to C, compiles to an executable file and starts that file.

    ./transpile-and-run-gcc.sh example.ass

The example allows to enter two values a and b. It then computes a^b and outputs that value.