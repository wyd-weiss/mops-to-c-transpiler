#include <stdio.h>

int main() {
    int a, b, c, d, e, f, g, h;
    int $0,$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35,$36,$37,$38,$39,$40,$41,$42,$43,$44,$45,$46,$47,$48,$49,$50,$51,$52,$53,$54,$55,$56,$57,$58,$59,$60,$61,$62,$63,$64,$65,$66,$67,$68,$69,$70,$71;
    int akku, cmp;

    // in a
    printf("Please enter value for a: ");
    scanf("%d", &a);
    // in b
    printf("Please enter value for b: ");
    scanf("%d", &b);
    // ld 1
    akku = 1;
    // st c
    c = akku;
compute:
    // ld b
    akku = b;
    // cmp 0
    if(akku < 0) {
      cmp = -1;
    } else if(akku > 0) {
      cmp = +1;
    } else {
      cmp = 0;
    }
    // jeq output
    if(cmp == 0) {
      goto output;
    }
    // sub 1
    akku -= 1;
    // st b
    b = akku;
    // ld c
    akku = c;
    // mul a
    akku *= a;
    // st c
    c = akku;
    // jmp compute
    goto compute;
output:
    // out c
    printf("Output: c is %d\n", c);
    // end
    return 0;
}