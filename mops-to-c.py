import sys
import argparse
import subprocess
import os
import re

# Function returns C label for given MOPS label (either term or line number)
# It also files a list of used line labels
def createGotoLabel(label, usedLineLabels):
    # Is this a jump to a line?
    if re.match("#\d+", label):
        # Yes, store this line and generate line jump label
        usedLineLabels.append(label[1:])
        return "$line_" + label[1:]
    else:
        # No, just a regular label
        return label

# Check command line parameters
if len(sys.argv) != 3:
    print("Usage: mops-to-c <input assembler filename> <output c filename>")
    print("Example: mops-to-c program.ass program.c")
    exit()

inputFilename = sys.argv[1]
outputFilename = sys.argv[2]

# Map from commands to number of parameters
commandParameterCount = {
    "ld" : 1,
    "st" : 1,
    "in" : 1,
    "out" : 1,
    "add" : 1,
    "sub" : 1,
    "mul" : 1,
    "div" : 1,
    "mod" : 1,
    "cmp" : 1,
    "jmp" : 1,
    "jlt" : 1,
    "jeq" : 1,
    "jgt" : 1,
    "end" : 0,
}

output = []

# Header
output.append("#include <stdio.h>")
output.append("")
output.append("int main() {")
output.append("    int a, b, c, d, e, f, g, h;")
output.append("    int " + ",".join(list(map(lambda num: "$" + str(num), range(0, 72)))) + ";")
output.append("    int akku, cmp;")
output.append("")

endCommandExists = False

# Process entire input file
lineCount = 0
usedLineLabels = []
with open(inputFilename, 'r') as inputFile:
    for line in inputFile.readlines():
        lineCount += 1
        line = line.replace("\n", "")

        # Remove comment
        commentIndex = line.find(";")
        if commentIndex != -1:
            line = line[:commentIndex]

        # Check for jump label
        jumpLabelIndex = line.find(":")
        if jumpLabelIndex != -1:
            jumpLabel = line[(jumpLabelIndex+1):]
            line = line[:jumpLabelIndex]
        else:
            jumpLabel = None

        # Skip empty lines
        line = line.strip()
        if not line:
            continue
            
        # Replace multiple spaces by single space
        line = re.sub(' +', ' ', line)

        # Split command and parameter
        items = line.split(" ")
        command = items[0].lower()
        parameter = None
        if len(items) > 1:
            parameter = items[1]

        # Check parameter count
        if not command in commandParameterCount:
            print("Unknown command {} in line: {}".format(command, line))
            exit(1)
        expectedCount = commandParameterCount[command]
        if len(items) - 1 != expectedCount:
            print("Command {} requires {} parameters, but line contains {}: {}".format(command, expectedCount, len(items) - 1, line))
            exit(1)

        # Add jump label
        if jumpLabel:
            output.append("{}:".format(jumpLabel))
        output.append("$line_{}:".format(lineCount))

        # Check command
        output.append("    // " + line)
        if command == "ld":
            output.append("    akku = " + parameter + ";")
        elif command == "st":
            output.append("    " + parameter + " = akku;")
        elif command == "in":
            output.append("    printf(\"Please enter value for " + parameter + ": \");")
            output.append("    scanf(\"%d\", &" + parameter + ");")
        elif command == "out":
            output.append("    printf(\"Output: " + parameter + " is %d\\n\", {});".format(parameter))
        elif command == "add":
            output.append("    akku += " + parameter + ";")
        elif command == "sub":
            output.append("    akku -= " + parameter + ";")
        elif command == "mul":
            output.append("    akku *= " + parameter + ";")
        elif command == "div":
            output.append("    akku /= " + parameter + ";")
        elif command == "mod":
            output.append("    akku %= " + parameter + ";")
        elif command == "cmp":
            output.append("    if(akku < " + parameter + ") {")
            output.append("      cmp = -1;")
            output.append("    } else if(akku > " + parameter + ") {")
            output.append("      cmp = +1;")
            output.append("    } else {")
            output.append("      cmp = 0;")
            output.append("    }")
        elif command == "jmp":
            output.append("    goto " + createGotoLabel(parameter, usedLineLabels) + ";")
        elif command == "jlt":
            output.append("    if(cmp == -1) {")
            output.append("      goto " + createGotoLabel(parameter, usedLineLabels) + ";")
            output.append("    }")
        elif command == "jeq":
            output.append("    if(cmp == 0) {")
            output.append("      goto " + createGotoLabel(parameter, usedLineLabels) + ";")
            output.append("    }")
        elif command == "jgt":
            output.append("    if(cmp == +1) {")
            output.append("      goto " + createGotoLabel(parameter, usedLineLabels) + ";")
            output.append("    }")
        elif command == "end":
            endCommandExists = True
            output.append("    return 0;")

    output.append("}")

# Ensure that end command exists
if not endCommandExists:
    print("end command missing.")
    exit(1)

# Remove unused jump labels
cleanedResult = []
for line in output:
    match = re.match("\$line_(\d*):", line)
    useLine = not match or (match.group(1) in usedLineLabels)
    if useLine:
        cleanedResult.append(line)

# Write result file
with open(outputFilename, 'w') as outputFile:
    outputFile.writelines("\n".join(cleanedResult))

exit(0)